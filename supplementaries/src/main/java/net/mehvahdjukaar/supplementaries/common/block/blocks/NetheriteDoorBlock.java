package net.mehvahdjukaar.supplementaries.common.block.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.DoorBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NetheriteDoorBlock extends DoorBlock {
	protected NetheriteDoorBlock( Settings settings ) {
		super( settings );
	}

	@Override
	public ActionResult onUse( BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit ) {
		return super.onUse( state, world, pos, player, hand, hit );
	}

	private int getCloseSound() {
		return 1011;
	}

	private int getOpenSound() {
		return 1005;
	}
}
