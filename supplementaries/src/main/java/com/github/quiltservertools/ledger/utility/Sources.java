package com.github.quiltservertools.ledger.utility;

import org.jetbrains.annotations.NotNull;


public final class Sources {
	@NotNull
	public static final String FIRE = "fire";
	@NotNull
	public static final String GRAVITY = "gravity";
	@NotNull
	public static final String EXPLOSION = "explosion";
	@NotNull
	public static final String PLAYER = "player";
	@NotNull
	public static final String BROKE = "broke";
	@NotNull
	public static final String DECAY = "decay";
	@NotNull
	public static final String DRY = "dry";
	@NotNull
	public static final String WET = "wet";
	@NotNull
	public static final String MELT = "melt";
	@NotNull
	public static final String FROST_WALKER = "frost_walker";
	@NotNull
	public static final String REDSTONE = "redstone";
	@NotNull
	public static final String FLUID = "fluid";
	@NotNull
	public static final String FILL = "fill";
	@NotNull
	public static final String DRAIN = "drain";
	@NotNull
	public static final String DRIP = "drip";
	@NotNull
	public static final String SNOW = "snow";
	@NotNull
	public static final String REMOVE = "remove";
	@NotNull
	public static final String TRAMPLE = "trample";
	@NotNull
	public static final String EXTINGUISH = "extinguish";
	@NotNull
	public static final String INSERT = "insert";
	@NotNull
	public static final String INTERACT = "interact";
	@NotNull
	public static final String CONSUME = "consume";
	@NotNull
	public static final String GROW = "grow";
	@NotNull
	public static final String SNOW_GOLEM = "snow_golem";
	@NotNull
	public static final String SPONGE = "sponge";
	@NotNull
	public static final String PORTAL = "portal";
	@NotNull
	public static final String COMMAND = "command";
	@NotNull
	public static final String UNKNOWN = "unknown";
}
