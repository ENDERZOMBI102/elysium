package com.github.quiltservertools.ledger.actions;

import com.mojang.authlib.GameProfile;
import net.minecraft.block.BlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;

public abstract class AbstractActionType implements ActionType {
	@NotNull
	private Instant timestamp;
	@NotNull
	private BlockPos pos;
	@Nullable
	private Identifier world;
	@NotNull
	private Identifier objectIdentifier;
	@NotNull
	private Identifier oldObjectIdentifier;
	@Nullable
	private BlockState blockState;
	@Nullable
	private BlockState oldBlockState;
	@NotNull
	private String sourceName;
	@Nullable
	private GameProfile sourceProfile;
	@Nullable
	private String extraData;
	private boolean rolledBack;

	@NotNull
	public Instant getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(@NotNull Instant var1) {
		this.timestamp = var1;
	}

	@NotNull
	public BlockPos getPos() {
		return this.pos;
	}

	public void setPos(@NotNull BlockPos var1) {
		this.pos = var1;
	}

	@Nullable
	public Identifier getWorld() {
		return this.world;
	}

	public void setWorld(@Nullable Identifier var1) {
		this.world = var1;
	}

	@NotNull
	public Identifier getObjectIdentifier() {
		return this.objectIdentifier;
	}

	public void setObjectIdentifier(@NotNull Identifier var1) {
		this.objectIdentifier = var1;
	}

	@NotNull
	public Identifier getOldObjectIdentifier() {
		return this.oldObjectIdentifier;
	}

	public void setOldObjectIdentifier(@NotNull Identifier var1) {
		this.oldObjectIdentifier = var1;
	}

	@Nullable
	public BlockState getBlockState() {
		return this.blockState;
	}

	public void setBlockState(@Nullable BlockState var1) {
		this.blockState = var1;
	}

	@Nullable
	public BlockState getOldBlockState() {
		return this.oldBlockState;
	}

	public void setOldBlockState(@Nullable BlockState var1) {
		this.oldBlockState = var1;
	}

	@NotNull
	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(@NotNull String var1) {
		this.sourceName = var1;
	}

	@Nullable
	public GameProfile getSourceProfile() {
		return this.sourceProfile;
	}

	public void setSourceProfile(@Nullable GameProfile var1) {
		this.sourceProfile = var1;
	}

	@Nullable
	public String getExtraData() {
		return this.extraData;
	}

	public void setExtraData(@Nullable String var1) {
		this.extraData = var1;
	}

	public boolean getRolledBack() {
		return this.rolledBack;
	}

	public void setRolledBack(boolean var1) {
		this.rolledBack = var1;
	}

	public boolean rollback(@NotNull MinecraftServer server) {
		return false;
	}

	public void previewRollback(@NotNull ServerPlayerEntity player) {
	}

	public void previewRestore(@NotNull ServerPlayerEntity player) {
	}

	public boolean restore(@NotNull MinecraftServer server) {
		return false;
	}

	@NotNull
	public Text getMessage() {
		throw new IllegalStateException();
	}

	@NotNull
	public Text getTimeMessage() {
		throw new IllegalStateException();
	}

	@NotNull
	public Text getActionMessage() {
		throw new IllegalStateException();
	}

	@NotNull
	public Text getObjectMessage() {
		throw new IllegalStateException();
	}

	@NotNull
	public Text getLocationMessage() {
		throw new IllegalStateException();
	}

	public AbstractActionType() {
		this.timestamp = Instant.now();
		this.pos = BlockPos.ORIGIN;
		this.objectIdentifier = new Identifier("air");
		this.oldObjectIdentifier = new Identifier("air");
		this.sourceName = "unknown";
	}
}
