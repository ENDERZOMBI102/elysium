package com.github.quiltservertools.ledger.actions;

import com.mojang.authlib.GameProfile;
import net.minecraft.block.BlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;

public interface ActionType {
	@NotNull
	String getIdentifier();

	@NotNull
	Instant getTimestamp();

	void setTimestamp(@NotNull Instant var1);

	@NotNull
	BlockPos getPos();

	void setPos(@NotNull BlockPos var1);

	@Nullable
	Identifier getWorld();

	void setWorld(@Nullable Identifier var1);

	@NotNull
	Identifier getObjectIdentifier();

	void setObjectIdentifier(@NotNull Identifier var1);

	@NotNull
	Identifier getOldObjectIdentifier();

	void setOldObjectIdentifier(@NotNull Identifier var1);

	@Nullable
	BlockState getBlockState();

	void setBlockState(@Nullable BlockState var1);

	@Nullable
	BlockState getOldBlockState();

	void setOldBlockState(@Nullable BlockState var1);

	@NotNull
	String getSourceName();

	void setSourceName(@NotNull String var1);

	@Nullable
	GameProfile getSourceProfile();

	void setSourceProfile(@Nullable GameProfile var1);

	@Nullable
	String getExtraData();

	void setExtraData(@Nullable String var1);

	boolean getRolledBack();

	void setRolledBack(boolean var1);

	boolean rollback(@NotNull MinecraftServer var1);

	boolean restore(@NotNull MinecraftServer var1);

	void previewRollback(@NotNull ServerPlayerEntity var1);

	void previewRestore(@NotNull ServerPlayerEntity var1);

	@NotNull
	String getTranslationType();

	@NotNull
	Text getMessage();
}
