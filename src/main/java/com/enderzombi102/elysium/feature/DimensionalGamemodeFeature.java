package com.enderzombi102.elysium.feature;

import com.enderzombi102.elysium.config.Config;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.fabricmc.fabric.api.entity.event.v1.ServerEntityWorldChangeEvents;
import net.fabricmc.fabric.api.entity.event.v1.ServerPlayerEvents;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.GameMode;

import static com.enderzombi102.elysium.Elysium.LOGGER;


public class DimensionalGamemodeFeature {
	public static void register() {
		ServerEntityWorldChangeEvents.AFTER_PLAYER_CHANGE_WORLD.register( DimensionalGamemodeFeature::afterChangeWorld );
		ServerPlayerEvents.AFTER_RESPAWN.register( DimensionalGamemodeFeature::afterRespawn );
	}

	private static void afterRespawn( ServerPlayerEntity oldPlayer, ServerPlayerEntity newPlayer, boolean oldAlive ) {
		if (! Config.get().features.forceDimensionalGamemode )
			return;

		if ( oldPlayer.getWorld() == newPlayer.getWorld() )
			return;

		afterChangeWorld( newPlayer, oldPlayer.getWorld(), newPlayer.getWorld() );
	}


	private static void afterChangeWorld( ServerPlayerEntity player, ServerWorld origin, ServerWorld destination ) {
		if (! Config.get().features.forceDimensionalGamemode )
			return;

		var dimension = destination.getRegistryKey().getValue().toString();
		var bypass = Permissions.check( player, "elysium.dimension_bypass" );

		if ( Config.get().logging.dimensionalGamemode )
			LOGGER.info(
				"[Elysium] Player `{}` switched to dimension `{}`, bypass gamemode change: {}",
				player.getEntityName(),
				dimension,
				bypass
			);

		if ( bypass )
			return;

		var defaultGamemode = Config.get().dimensionGamemodes.get( "elysium:default" );

		if ( defaultGamemode == null ) {
			LOGGER.error( "[Elysium] No `elysium:default` gamemode was set! `dimensionGamemodes` will do nothing" );
			return;
		}

		var modeName = Config.get().dimensionGamemodes.getOrDefault( dimension, defaultGamemode );

		if ( Config.get().logging.dimensionalGamemode )
			LOGGER.debug(
				"[Elysium] Teleported to: `{}`, dimension's gamemode: `{}`, default gamemode: `{}`",
				dimension, modeName, defaultGamemode
			);

		player.changeGameMode( GameMode.byName( modeName ) );
	}
}
