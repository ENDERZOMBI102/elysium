package com.enderzombi102.elysium.screen.widget;

import com.glisco.numismaticoverhaul.currency.Currency;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import io.wispforest.owo.ui.core.Color;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import static io.github.cottonmc.cotton.gui.widget.WLabel.DEFAULT_TEXT_COLOR;

@SuppressWarnings( "NotNullFieldNotInitialized" )
public class WMoneyShowWidget extends WWidget {
	public static final ItemStack GOLD_STACK = new ItemStack( Currency.GOLD.asItem() );
	public static final ItemStack SILVER_STACK = new ItemStack( Currency.SILVER.asItem() );
	public static final ItemStack BRONZE_STACK = new ItemStack( Currency.BRONZE.asItem() );

	private @NotNull Text gold;
	private @NotNull Text silver;
	private @NotNull Text bronze;
	private long money;
	private long minimum = -1;

	public WMoneyShowWidget( long amount, long minimum ) {
		this.setAmount( amount );
		this.minimum = minimum;
	}

	@Override
	public void paint( MatrixStack matrices, int x, int y, int mouseX, int mouseY ) {
		// interfaces
		var textRenderer = MinecraftClient.getInstance().textRenderer;
		var itemRenderer = MinecraftClient.getInstance().getItemRenderer();
		// working vars
		int size;
		int offset = 0;
		// draw
		size = textRenderer.getWidth( this.gold );
		textRenderer.draw( matrices, this.gold, x, y + 2, this.money < this.minimum ? Color.RED.rgb() : DEFAULT_TEXT_COLOR );
		itemRenderer.renderGuiItemIcon( GOLD_STACK, x + size - 3, y - 3 );

		offset += size + 16;
		size = textRenderer.getWidth( this.silver );
		textRenderer.draw( matrices, this.silver, x + offset, y + 2, this.money < this.minimum ? Color.RED.rgb() : DEFAULT_TEXT_COLOR );
		itemRenderer.renderGuiItemIcon( SILVER_STACK, x + offset + size - 3, y - 3 );

		offset += size + 16;
		size = textRenderer.getWidth( this.bronze );
		textRenderer.draw( matrices, this.bronze, x + offset, y + 2, this.money < this.minimum ? Color.RED.rgb() : DEFAULT_TEXT_COLOR );
		itemRenderer.renderGuiItemIcon( BRONZE_STACK, x + offset + size - 3, y - 3 );
	}

	public void setAmount( long amount ) {
		this.money = amount;

		this.gold = Text.literal( String.valueOf( amount / 10000L ) );
		amount = amount % 10000L;

		this.silver = Text.literal( String.valueOf( amount / 100L ) );
		amount = amount % 100L;

		this.bronze = Text.literal( String.valueOf( amount ) );
	}

	public long getAmount() {
		return this.money;
	}

	public void setMinimum( long minimum ) {
		this.minimum = minimum;
	}
}
