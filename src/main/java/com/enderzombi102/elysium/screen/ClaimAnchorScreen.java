package com.enderzombi102.elysium.screen;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;

public class ClaimAnchorScreen extends CottonInventoryScreen<ClaimAnchorGuiDescription> {
	public ClaimAnchorScreen( ClaimAnchorGuiDescription gui, PlayerEntity player, Text title ) {
		super( gui, player, title );
	}
}
