package com.enderzombi102.elysium.client;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.feature.AntiCheatFeature;
import com.enderzombi102.elysium.feature.ClaimFeature;
import com.enderzombi102.elysium.registry.ScreenRegistry;
import net.devtech.arrp.api.RRPCallback;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;


public class ElysiumClient implements ClientModInitializer {
	@Override
	public void onInitializeClient() {
		// registries
		ScreenRegistry.registerClient();

		// features
		ClaimFeature.registerClient();
		AntiCheatFeature.registerClient();


		// resource packs
		RRPCallback.AFTER_VANILLA.register( resources -> {
			resources.add( new ElysiumResourcePack( "ElysiumCraft resources", "resourcepack" ) );;
		});

		// config sync
		ClientPlayNetworking.registerGlobalReceiver( CONFIG_SYNC_ID, ( client, networkHandler, data, sender ) -> {
			var version = data.readString();
			var config = data.readString();

			client.execute( () -> Config.loadFromPacket( version, config ) );
		});
	}
}
