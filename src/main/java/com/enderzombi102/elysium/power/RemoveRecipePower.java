package com.enderzombi102.elysium.power;

import io.github.apace100.apoli.power.Power;
import io.github.apace100.apoli.power.PowerType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RemoveRecipePower extends Power {
	private final List<Identifier> recipes;

	public RemoveRecipePower( PowerType<?> type, LivingEntity entity, List<Identifier> recipes ) {
		super( type, entity );
		this.recipes = recipes;
	}

	public boolean blocks( @NotNull Identifier recipe ) {
		return recipes.stream().anyMatch( recipe::equals );
	}
}
