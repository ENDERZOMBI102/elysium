package com.enderzombi102.elysium.ledger;

import com.github.quiltservertools.ledger.actions.AbstractActionType;
import com.github.quiltservertools.ledger.utility.Sources;
import draylar.goml.api.Claim;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.NotNull;

public class ClaimDropActionType extends AbstractActionType {
	public ClaimDropActionType( Claim claim ) {
		this.setPos( claim.getOrigin() );
		this.setWorld( claim.getWorld() );
		this.setObjectIdentifier( Registry.BLOCK.getId( claim.getType() ) );
		this.setSourceName( Sources.REMOVE );
		this.setSourceProfile( null );
		this.setExtraData( null );
	}

	@Override
	public @NotNull String getIdentifier() {
		return "elysium-claim-drop";
	}

	@Override
	public @NotNull String getTranslationType() {
		return "block";
	}
}
