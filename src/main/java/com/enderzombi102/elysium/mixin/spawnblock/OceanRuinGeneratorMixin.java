package com.enderzombi102.elysium.mixin.spawnblock;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.Util;
import net.minecraft.structure.OceanRuinGenerator;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.ServerWorldAccess;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( OceanRuinGenerator.Piece.class )
public class OceanRuinGeneratorMixin {
	@Inject(
		method = "handleMetadata",
		at = @At(
			value = "HEAD",
			target = "Lnet/minecraft/entity/EntityType;create(Lnet/minecraft/world/World;)Lnet/minecraft/entity/Entity;"
		),
		cancellable = true
	)
	public void clearOverworldFromMobs( String metadata, BlockPos pos, ServerWorldAccess world, Random random, BlockBox boundingBox, CallbackInfo ci ) {
		if ( Config.get().features.disableMobSpawning && Util.isOverworld( world.toServerWorld() ) )
			ci.cancel();
	}
}
