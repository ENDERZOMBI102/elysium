package com.enderzombi102.elysium.mixin.spawnblock;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.Util;
import net.minecraft.structure.StructureTemplate;
import net.minecraft.util.BlockMirror;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ServerWorldAccess;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( StructureTemplate.class )
public class StructureTemplateMixin {
	@Inject(
		method = "spawnEntities",
		at = @At( "HEAD" ),
		cancellable = true
	)
	public void clearOverworldFromMobs( ServerWorldAccess world, BlockPos pos, BlockMirror mirror, BlockRotation rotation, BlockPos pivot, BlockBox area, boolean initializeMobs, CallbackInfo ci ) {
		if ( Config.get().features.disableMobSpawning && Util.isOverworld( world.toServerWorld() ) )
			ci.cancel();
	}
}
