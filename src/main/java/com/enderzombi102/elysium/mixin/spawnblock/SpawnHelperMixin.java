package com.enderzombi102.elysium.mixin.spawnblock;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.Util;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.SpawnHelper;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( SpawnHelper.class )
public class SpawnHelperMixin {
	@Inject(
		method = "populateEntities",
		at = @At( "HEAD" ),
		cancellable = true
	)
	private static void clearOverworldFromMobs( ServerWorldAccess world, RegistryEntry<Biome> biomeEntry, ChunkPos chunkPos, Random random, CallbackInfo ci ) {
		if ( Config.get().features.disableMobSpawning && Util.isOverworld( world.toServerWorld() ) )
			ci.cancel();
	}

	@Inject(
		method = "spawnEntitiesInChunk(Lnet/minecraft/entity/SpawnGroup;Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/world/chunk/Chunk;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/world/SpawnHelper$Checker;Lnet/minecraft/world/SpawnHelper$Runner;)V",
		at = @At( "HEAD" ),
		cancellable = true
	)
	private static void clearOverworldFromMobs( SpawnGroup group, ServerWorld world, Chunk chunk, BlockPos pos, SpawnHelper.Checker checker, SpawnHelper.Runner runner, CallbackInfo ci ) {
		if ( Config.get().features.disableMobSpawning && Util.isOverworld( world ) )
			ci.cancel();
	}
}
