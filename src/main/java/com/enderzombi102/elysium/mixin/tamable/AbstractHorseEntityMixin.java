package com.enderzombi102.elysium.mixin.tamable;

import com.enderzombi102.elysium.imixin.TameableInjector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AbstractHorseEntity;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

import java.util.UUID;

@Mixin( AbstractHorseEntity.class )
public abstract class AbstractHorseEntityMixin implements TameableInjector {
	@Override
	@Unique
	public @Nullable UUID getOwnerUuid() {
		return ( (AbstractHorseEntity) (Object) this ).getOwnerUuid();
	}

	@Override
	public @Nullable Entity getOwner() {
		var owner = this.getOwnerUuid();
		return owner == null ? null : ( (MobEntity) (Object) this ).world.getPlayerByUuid( owner );
	}
}
