package com.enderzombi102.elysium.mixin.tamable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Tameable;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.scoreboard.AbstractTeam;
import net.minecraft.server.ServerConfigHandler;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Optional;
import java.util.UUID;

@SuppressWarnings( "WrongEntityDataParameterClass" )
@Mixin( BoatEntity.class )
public abstract class BoatEntityMixin extends Entity implements Tameable {
	public BoatEntityMixin( EntityType<?> type, World world ) {
		super( type, world );
	}

	@Unique
	private static final TrackedData<Byte> TAMEABLE_FLAGS = DataTracker.registerData(
		BoatEntity.class,
		TrackedDataHandlerRegistry.BYTE
	);
	@Unique
	private static final TrackedData< Optional<UUID> > OWNER_UUID = DataTracker.registerData(
		BoatEntity.class,
		TrackedDataHandlerRegistry.OPTIONAL_UUID
	);

	@Inject( method = "initDataTracker", at = @At( "TAIL" ) )
	protected void onInitDataTracker( CallbackInfo ci ) {
		this.dataTracker.startTracking( TAMEABLE_FLAGS, (byte) 0 );
		this.dataTracker.startTracking( OWNER_UUID, Optional.empty() );
	}

	@Inject( method = "writeCustomDataToNbt", at = @At( "TAIL" ) )
	public void writeTamableData( NbtCompound nbt, CallbackInfo ci ) {
		if ( this.getOwnerUuid() != null )
			nbt.putUuid( "Owner", this.getOwnerUuid() );
	}

	@Inject( method = "readCustomDataFromNbt", at = @At( "TAIL" ) )
	public void readTamableData( NbtCompound nbt, CallbackInfo ci ) {
		UUID uuid;
		if ( nbt.containsUuid( "Owner" ) )
			uuid = nbt.getUuid( "Owner" );
		else
			uuid = ServerConfigHandler.getPlayerUuidByName( this.getServer(), nbt.getString( "Owner" ) );

		if ( uuid != null ) {
			try {
				this.setOwnerUuid( uuid );
				this.setTamed( true );
			} catch ( Throwable e ) {
				this.setTamed( false );
			}
		}
	}

	@Unique
	public boolean isTamed() {
		return ( this.dataTracker.get( TAMEABLE_FLAGS ) & 4 ) != 0;
	}

	@Unique
	public void setTamed( boolean tamed ) {
		byte b = this.dataTracker.get( TAMEABLE_FLAGS );
		if ( tamed ) {
			this.dataTracker.set( TAMEABLE_FLAGS, (byte) ( b | 4 ) );
		} else {
			this.dataTracker.set( TAMEABLE_FLAGS, (byte) ( b & -5 ) );
		}
	}

	public @Nullable UUID getOwnerUuid() {
		return this.dataTracker.get( OWNER_UUID ).orElse( null );
	}

	@Unique
	public void setOwnerUuid( @Nullable UUID uuid ) {
		this.dataTracker.set( OWNER_UUID, Optional.ofNullable( uuid ) );
	}

	@Unique
	public void setOwner( PlayerEntity player ) {
		this.setTamed( true );
		this.setOwnerUuid( player.getUuid() );
	}

	@Nullable
	public LivingEntity getOwner() {
		try {
			UUID uUID = this.getOwnerUuid();
			return uUID == null ? null : this.world.getPlayerByUuid( uUID );
		} catch ( IllegalArgumentException var2 ) {
			return null;
		}
	}

	@Unique
	public boolean isOwner( LivingEntity entity ) {
		return entity == this.getOwner();
	}

	public AbstractTeam getScoreboardTeam() {
		if ( this.isTamed() ) {
			var livingEntity = this.getOwner();
			if ( livingEntity != null )
				return livingEntity.getScoreboardTeam();
		}

		return super.getScoreboardTeam();
	}

	public boolean isTeammate( Entity other ) {
		if ( this.isTamed() ) {
			var livingEntity = this.getOwner();
			if ( other == livingEntity )
				return true;

			if ( livingEntity != null )
				return livingEntity.isTeammate( other );
		}

		return super.isTeammate( other );
	}
}
