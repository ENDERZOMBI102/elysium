package com.enderzombi102.elysium.mixin.supplementaries;

import com.enderzombi102.elysium.util.Util;
import com.magistuarmory.item.MedievalBowItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.mk.archers_arsenal.items.ModBowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( { ModBowItem.class, MedievalBowItem.class } )
public abstract class BowItemsMixin extends BowItem {
	public BowItemsMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "onStoppedUsing",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/item/ItemStack;decrement(I)V"
		)
	)
	private void shrinkQuiverArrow( ItemStack stack, World level, LivingEntity livingEntity, int timeCharged, CallbackInfo ci ) {
		var data = Util.getQuiverData( (PlayerEntity) livingEntity );
		if ( data != null )
			data.consumeArrow();
	}
}
