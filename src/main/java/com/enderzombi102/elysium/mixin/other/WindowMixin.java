package com.enderzombi102.elysium.mixin.other;

import net.minecraft.client.util.Window;
import org.lwjgl.glfw.GLFW;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( Window.class )
public class WindowMixin {
	@Shadow
	@Final
	private long handle;

	@Inject(
		method = "setTitle",
		at = @At( "HEAD" ),
		cancellable = true
	)
	public void modifyWindowTitle( String title, CallbackInfo ci ) {
		GLFW.glfwSetWindowTitle( this.handle, "ElysiumCraft RP" );
		ci.cancel();
	}
}
