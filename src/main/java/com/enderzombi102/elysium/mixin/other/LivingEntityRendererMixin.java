package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import io.github.apace100.apoli.component.PowerHolderComponent;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( value = LivingEntityRenderer.class, priority = 1100 )
public abstract class LivingEntityRendererMixin< T extends LivingEntity, M extends EntityModel<T> > extends EntityRenderer<T> implements FeatureRendererContext<T, M> {
	protected LivingEntityRendererMixin( EntityRendererFactory.Context ctx ) {
		super( ctx );
	}

	@Inject(
		method = "hasLabel(Lnet/minecraft/entity/LivingEntity;)Z",
		at = @At(
			value = "INVOKE",
			target = "net/minecraft/client/network/ClientPlayerEntity.getScoreboardTeam()Lnet/minecraft/scoreboard/AbstractTeam;",
			shift = At.Shift.AFTER
		),
		cancellable = true
	)
	public void hideNamePlate( T livingEntity, CallbackInfoReturnable<Boolean> cir ) {
		var player = MinecraftClient.getInstance().player;
		assert player != null : "Why are we rendering the player when we're not playing??";

		var component = PowerHolderComponent.KEY.get( player );
		if ( component.hasPower( PowerTypeRegistry.HIDE_NAME_PLATES ) && !component.getPower( PowerTypeRegistry.HIDE_NAME_PLATES ).isActive() )
			return;

		cir.setReturnValue( false );
	}

	@SuppressWarnings( { "MixinAnnotationTarget", "UnresolvedMixinReference" } )
	@Inject(
		method = "playerex_shouldRenderLevel",
		at = @At(
			value = "INVOKE",
			target = "net/minecraft/client/network/ClientPlayerEntity.getScoreboardTeam()Lnet/minecraft/scoreboard/AbstractTeam;",
			shift = At.Shift.AFTER,
			remap = true
		),
		cancellable = true,
		remap = false,
		require = 0
	)
	public void hideLevelPlate( T livingEntity, CallbackInfoReturnable<Boolean> cir ) {
		var player = MinecraftClient.getInstance().player;
		assert player != null : "Why are we rendering the player when we're not playing??";

		var component = PowerHolderComponent.KEY.get( player );
		if ( component.hasPower( PowerTypeRegistry.HIDE_NAME_PLATES ) && !component.getPower( PowerTypeRegistry.HIDE_NAME_PLATES ).isActive() )
			return;

		cir.setReturnValue( false );
	}
}
