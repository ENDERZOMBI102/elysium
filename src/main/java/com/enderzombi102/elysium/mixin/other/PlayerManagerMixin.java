package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.CompRegistry;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( PlayerManager.class )
public class PlayerManagerMixin {
	@Inject( method = "remove", at = @At( "HEAD" ) )
	public void rememberLeaveTime( ServerPlayerEntity player, CallbackInfo ci ) {
		assert Elysium.server != null : "Running server code on a client? wut";
		CompRegistry.ELYSIUM_LEVEL
			.get( Elysium.server.getSaveProperties() )
			.setLastPlayerTime( player.getUuid(), System.currentTimeMillis() );
	}
}
