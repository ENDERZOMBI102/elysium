package com.enderzombi102.elysium.mixin.potion;

import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.potion.Potion;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.spell_power.SpellPowerMod;
import net.spell_power.internals.SpellStatusEffect;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin( SpellPowerMod.class )
public class SpellPowerModMixin {
	@Inject(
		method = "registerStatusEffects",
		at = {
			@At(
				value = "INVOKE",
				target = "Lnet/minecraft/util/registry/Registry;register(Lnet/minecraft/util/registry/Registry;Lnet/minecraft/util/Identifier;Ljava/lang/Object;)Ljava/lang/Object;"
			),
			@At(
				value = "INVOKE",
				target = "Lnet/minecraft/util/registry/Registry;register(Lnet/minecraft/util/registry/Registry;ILjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;"
			)
		},
		locals = LocalCapture.CAPTURE_FAILHARD
	)
	private static void afterStatusEffectRegistration( CallbackInfo ci, @Local( index = 3 ) LocalRef<Identifier> id, @Local( index = 4 ) LocalRef<SpellStatusEffect> effect ) {
		Registry.register(
			Registry.POTION,
			id.get(),
			new Potion( "sp_" + id.get().getPath(), new StatusEffectInstance( effect.get(), 600 ) )
		);
	}
}
