package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.registry.CompRegistry;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Util.isAdminClaim;

@Mixin( ClaimAnchorBlock.class )
public abstract class ClaimAnchorBlockMixin extends Block implements BlockEntityProvider {
	private ClaimAnchorBlockMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "onPlaced",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/event/ClaimEvents$GenericClaimEvent;onEvent(Ldraylar/goml/api/Claim;)V",
			shift = At.Shift.AFTER,
			remap = false
		)
	)
	public void afterPlaced( World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack, CallbackInfo ci ) {
		if (! Config.get().features.claimModifications )
			return;

		if ( placer instanceof ServerPlayerEntity player ) {
			player.sendMessage( Text.translatable( "text.elysium.claim.welcome" ) );

			assert Elysium.server != null : "Why it nul????";
			CompRegistry.ELYSIUM_LEVEL
				.get( Elysium.server.getSaveProperties() )
				.setAnchorStamp( player.getUuid(), 0 );
			LOGGER.info( "[Elysium] Anchor placed, {}'s backup timestamp reset", placer.getEntityName() );
		}
	}

	@Inject(
		method = "onUse",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/Claim;openUi(Lnet/minecraft/server/network/ServerPlayerEntity;)V",
			shift = At.Shift.BEFORE
		),
		cancellable = true
	)
	public void openNewScreen( BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit, CallbackInfoReturnable<ActionResult> cir ) {
		if (! Config.get().features.claimModifications )
			return;

		// accounts for admin claim
		if ( isAdminClaim( state ) )
			return;

		// allow admins to open og screen when sneaking // NOTE: This won't work, as GOML ignores shift-clicks!
		if ( player.isSneaking() && player.getScoreboardTags().contains( "admin" ) )
			return;

		player.openHandledScreen( state.createScreenHandlerFactory( world, pos ) );
		cir.setReturnValue( ActionResult.SUCCESS );
	}

	@Nullable
	@Override
	public NamedScreenHandlerFactory createScreenHandlerFactory( BlockState state, World world, BlockPos pos ) {
		var entity = world.getBlockEntity( pos );
		assert entity instanceof ClaimAnchorBlockEntity : "Somehow, this block entity is not ours..?";
		return (NamedScreenHandlerFactory) entity;
	}
}
