package com.enderzombi102.elysium.mixin.goml;

import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import draylar.goml.EventHandlers;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.stream.Collectors;

import static com.enderzombi102.elysium.util.Util.isAdminClaim;

@SuppressWarnings( "UnstableApiUsage" )
@Mixin( EventHandlers.class )
public class EventHandlersMixin {
	@Inject(
		method = "lambda$registerInteractBlockCallback$4",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/EventHandlers;testPermission(Lcom/jamieswhiteshirt/rtree3i/Selection;Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/Hand;Lnet/minecraft/util/math/BlockPos;Ldraylar/goml/api/PermissionReason;)Lnet/minecraft/util/ActionResult;",
			shift = At.Shift.BEFORE
		),
		cancellable = true,
		locals = LocalCapture.CAPTURE_FAILHARD
	)
	private static void allowIntersectionalInteractions( PlayerEntity player, World world, Hand hand, BlockHitResult hit, CallbackInfoReturnable<ActionResult> cir, Selection< Entry<ClaimBox, Claim> > claimsFound ) {
		var claims = claimsFound.collect( Collectors.toList() );

		Claim closest = null;
		var closestDistance = 999999;
		for ( var entry : claims ) {
			var distance = entry.getValue().getOrigin().getManhattanDistance( hit.getBlockPos() );
			if ( distance < closestDistance ) {
				closest = entry.getValue();
				closestDistance = distance;
			}
		}

		if ( closest != null && isAdminClaim( closest ) )
			cir.setReturnValue( closest.hasPermission( player ) ? ActionResult.PASS : ActionResult.FAIL );
	}
}
