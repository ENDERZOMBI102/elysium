package com.enderzombi102.elysium.mixin.goml;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import draylar.goml.api.Claim;
import draylar.goml.block.SelectiveClaimAugmentBlock;
import draylar.goml.block.augment.AngelicAuraAugmentBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( AngelicAuraAugmentBlock.class )
public class AngelicAuraAugmentBlockMixin extends SelectiveClaimAugmentBlock {
	public AngelicAuraAugmentBlockMixin( String key, Settings settings, String texture ) {
		super( key, settings, texture );
	}

	@ModifyExpressionValue(
		method = "playerTick",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/block/augment/AngelicAuraAugmentBlock;canApply(Ldraylar/goml/api/Claim;Lnet/minecraft/entity/player/PlayerEntity;)Z"
		)
	)
	public boolean disallowUntrustedRegeneration( boolean og, Claim claim, PlayerEntity player ) {
		var uuid = player.getUuid();
		return claim.getTrusted().contains( uuid ) || claim.getOwners().contains( uuid );
	}

	@Override
	public void openSettings( Claim claim, ServerPlayerEntity player, @Nullable Runnable closeCallback ) {
		// override to do nothing :3
	}
}
