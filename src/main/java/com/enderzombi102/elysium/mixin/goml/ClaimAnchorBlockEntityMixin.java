package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.enderzombi102.elysium.screen.ClaimAnchorGuiDescription;
import com.enderzombi102.elysium.util.TaxesUtil;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.apfloat.Apfloat;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.UUID;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Util.isAdminClaim;


@Mixin( ClaimAnchorBlockEntity.class )
public abstract class ClaimAnchorBlockEntityMixin extends BlockEntity implements ExtendedScreenHandlerFactory, ElysiumClaimBlockEntity {
	public ClaimAnchorBlockEntityMixin( BlockEntityType<?> type, BlockPos pos, BlockState state ) {
		super( type, pos, state );
	}

	@Inject(
		method = "writeNbt",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/entity/BlockEntity;writeNbt(Lnet/minecraft/nbt/NbtCompound;)V",
			shift = At.Shift.BEFORE
		)
	)
	private void saveAdditionalData( NbtCompound tag, CallbackInfo ci ) {
		tag.putLong( "Elysium$Time", this.elysium$time );
	}

	@Inject(
		method = "readNbt",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/entity/BlockEntity;readNbt(Lnet/minecraft/nbt/NbtCompound;)V",
			shift = At.Shift.BEFORE
		)
	)
	private void loadAdditionalData( NbtCompound tag, CallbackInfo ci ) {
		if (! tag.contains( "Elysium$Time" ) )
			LOGGER.warn( "[Elysium] Anchor at {} is missing time data!!!", this.pos );
		this.elysium$time = tag.getLong( "Elysium$Time" );
	}

	@Inject(
		method = "tick",
		at = @At(
			value = "FIELD",
			target = "Ldraylar/goml/block/entity/ClaimAnchorBlockEntity;claim:Ldraylar/goml/api/Claim;",
			shift = At.Shift.BEFORE,
			ordinal = 2
		),
		locals = LocalCapture.CAPTURE_FAILHARD,
		cancellable = true
	)
	private static <T extends BlockEntity> void dropIfTooLate( World eWorld, BlockPos pos, BlockState state, T entity, CallbackInfo ci, ServerWorld world, ClaimAnchorBlockEntity anchor ) {
		if (! Config.get().features.claimModifications )
			return;

		var config = Config.get().claims.taxes;

		// accounts for admin claim and disabling
		if ( isAdminClaim( state ) || config.anchorDropDays == -1 )
			return;

		var claim = anchor.getClaim();
		assert claim != null : "Wat";

		var self = (ClaimAnchorBlockEntityMixin) (Object) anchor;

		// try to cache the current owner's UUID
		if ( self.elysium$owner == null )
			self.elysium$owner = claim
				.getOwners()
				.stream()
				.findFirst()
				.orElse( null );

		if ( self.elysium$owner != null ) {
			assert Elysium.server != null : "Server is null when it shouldn't be";
			var comp = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() );

			// check if the timer was corrupted
			if ( self.elysium$time != 0 && comp.getAnchorStamp( self.elysium$owner ) == 0 ) {
				LOGGER.warn( "[Elysium] Backup timestamp not valid, setting backup for {}", pos.toShortString() );
				comp.setAnchorStamp( self.elysium$owner, self.elysium$time );
			}

			if ( self.elysium$time != comp.getAnchorStamp( self.elysium$owner ) ) {
				LOGGER.warn( "[Elysium] Using backup timestamp for {}'s anchor at {}", self.elysium$owner, pos.toShortString() );
				self.elysium$time = comp.getAnchorStamp( self.elysium$owner );
			}

			if ( self.elysium$time < System.currentTimeMillis() ) {
				// avoid to destroy the block if the player is playing
				if ( Elysium.server.getPlayerManager().getPlayer( self.elysium$owner ) != null )
					return;

				var lastAccess = comp.getLastPlayerTime( self.elysium$owner );

				// millis -> secs -> mins -> hours -> days
				var passedDays = ( System.currentTimeMillis() - lastAccess ) / 1000d / 60 / 60 / 24;

				if ( passedDays > config.anchorDropDays ) {
					world.breakBlock( pos, !config.anchorDropDestroys );
					LOGGER.warn( "[Elysium-Ledger] Broken claim anchor at {} (owner: {})", pos.toShortString(), self.elysium$owner );
					ci.cancel();
				}
			}
		}
	}

	@Inject( method = "from", at = @At( "TAIL" ), remap = false )
	public void rememberPreUpgradeValues( ClaimAnchorBlockEntity old, CallbackInfo ci ) {
		assert old != null : "Why are we transferring from an non-existent entity? wtf";
		var mixin = (ClaimAnchorBlockEntityMixin) (Object) old;

		this.elysium$time = mixin.elysium$time;
		this.elysium$owner = mixin.elysium$owner;
	}

	@Unique
	private long elysium$time = 0;
	@Unique
	private UUID elysium$owner = null;

	// ExtendedScreenHandler
	@Override
	public Text getDisplayName() {
		// Using the block name as the screen title
		return Text.translatable( "screen.elysium.claim.title" );
	}

	@Override
	public ScreenHandler createMenu( int syncId, PlayerInventory inventory, PlayerEntity player ) {
		if ( Config.get().logging.claimAnchorScreen )
			LOGGER.info( "[Elysium] Opening modified claim anchor screen for `@{}`", player.getEntityName() );

		var buf = PacketByteBufs.create();
		this.writeScreenOpeningData( (ServerPlayerEntity) player, buf );
		return new ClaimAnchorGuiDescription( syncId, inventory, buf, ScreenHandlerContext.create( this.world, this.pos ) );
	}

	@Override
	public void writeScreenOpeningData( ServerPlayerEntity player, PacketByteBuf buf ) {
		buf.writeLong( this.elysium$time );
		buf.writeInt( TaxesUtil.getDistance( this.pos ) );
		buf.writeString( TaxesUtil.getClaimPup( this.pos ).toString() );
	}

	// ElysiumClaimAnchorBlockEntity
	@Override
	public void elysium$addTime( long amount ) {
		assert Elysium.server != null : "Running server code on a client? wut";
		CompRegistry.IMPERIUM_LEVEL.get( Elysium.server.getSaveProperties() ).addBalance( amount );

		var currentTime = System.currentTimeMillis();

		if ( this.elysium$time < currentTime )
			this.elysium$time = currentTime;

		var pup = TaxesUtil.getClaimPup( this.getPos() );

		var toAdd = new Apfloat( 86400 )
			.divide( pup )
			.multiply( new Apfloat( amount ) )
			.multiply( new Apfloat( 1000 ) );

		this.elysium$time += toAdd.longValue();
		this.markDirty();

		if ( this.elysium$owner != null )
			CompRegistry.ELYSIUM_LEVEL
				.get( Elysium.server.getSaveProperties() )
				.setAnchorStamp( this.elysium$owner, this.elysium$time );
		else
			LOGGER.warn( "[Elysium] Owner was somehow null at {}", this.getPos() );
	}

	@Override
	public long elysium$getTime() {
		return this.elysium$time;
	}
}
