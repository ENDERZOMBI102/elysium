package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.config.Config;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.item.ClaimAnchorBlockItem;
import draylar.goml.registry.GOMLBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemPlacementContext;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import static com.enderzombi102.elysium.Elysium.LOGGER;

@Mixin( ClaimAnchorBlockItem.class )
public abstract class ClaimAnchorBlockItemMixin {
	@Final
	@Shadow( remap = false )
	private ClaimAnchorBlock claimBlock;

	@ModifyExpressionValue(
		method = "canPlace",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/ClaimUtils;getClaimsInBox(Lnet/minecraft/world/WorldView;Lcom/jamieswhiteshirt/rtree3i/Box;)Lcom/jamieswhiteshirt/rtree3i/Selection;"
		)
	)
	private Selection< Entry<ClaimBox, Claim> > disallowOverlapping( Selection< Entry<ClaimBox, Claim> > claims, ItemPlacementContext ctx, BlockState state ) {
		// accounts for admin claim
		if ( this.claimBlock == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst() || !Config.get().features.dreamingGomlFixes )
			return claims;

		var origin = ctx.getBlockPos();

		var claimBox = ClaimUtils
			.createClaimBox( origin, Config.get().claims.distanceRadiusBetween )
			.rtree3iBox();

		return ClaimUtils
			.getClaimsInBox( ctx.getWorld(), claimBox )
			.filter( it -> {
				var otherOrigin = it.getValue().getOrigin();

				var distance = Math.max(
					Math.abs( origin.getX() - otherOrigin.getX() ),
					Math.abs( origin.getZ() - otherOrigin.getZ() )
				);

				if ( Config.get().logging.claimAnchor )
					LOGGER.info( "[Elysium] Distance: {}", distance );

				return distance <= Config.get().claims.distanceRadiusBetween;
			});
	}
}
