package com.enderzombi102.elysium.mixin.goml;

import com.jamieswhiteshirt.rtree3i.Entry;
import com.mojang.brigadier.context.CommandContext;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.other.ClaimCommand;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.CLAIM_ADMIN_MODER_ID;

@SuppressWarnings( "UnstableApiUsage" )
@Mixin( ClaimCommand.class )
public class ClaimCommandMixin {
	@Inject(
		method = "lambda$escape$11",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/server/network/ServerPlayerEntity;teleport(DDD)V",
			remap = true
		),
		remap = false
	)
	private static void logEscape( ServerPlayerEntity player, Entry<ClaimBox, Claim> claim, CallbackInfo ci ) {
		var vehicle = player.getVehicle();

		LOGGER.warn(
			"[Elysium-Ledger] Player {} escaped claim from {}{}",
			player.getEntityName(),
			player.getBlockPos().toShortString(),
			vehicle == null ? "" : " riding " + Registry.ENTITY_TYPE.getId( vehicle.getType() )
		);
	}

	@Inject(
		method = "adminMode",
		at = @At( "TAIL" ),
		locals = LocalCapture.CAPTURE_FAILHARD,
		remap = false
	)
	private static void sendAdminModerUpdate( CommandContext<ServerCommandSource> ctx, CallbackInfoReturnable<Integer> cir, ServerPlayerEntity player, boolean adminModeActive ) {
		var buf = PacketByteBufs.create();
		buf.writeBoolean( adminModeActive );
		ServerPlayNetworking.send( player, CLAIM_ADMIN_MODER_ID, buf );
	}
}
