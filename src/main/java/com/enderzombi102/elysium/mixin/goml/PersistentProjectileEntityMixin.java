package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.config.Config;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.util.hit.EntityHitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( PersistentProjectileEntity.class )
public class PersistentProjectileEntityMixin {
	@Inject(
		method = "onEntityHit",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/util/math/Vec3d;length()D"
		)
	)
	private void disableProjectileDamage( EntityHitResult entityHitResult, CallbackInfo info ) {
		if (! Config.get().features.dreamingGomlFixes )
			return;

		var persistentProjectileEntity = (PersistentProjectileEntity) (Object) this;
		var owner = persistentProjectileEntity.getOwner();

		if ( owner != null && owner.isPlayer() ) {
			var claims = ClaimUtils.getClaimsAt( persistentProjectileEntity.world, persistentProjectileEntity.getBlockPos() );
			if (! claims.isEmpty() ) {
				persistentProjectileEntity.setDamage( 0 );
				persistentProjectileEntity.setCritical( false );
			}
		}
	}
}
