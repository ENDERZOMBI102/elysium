package com.enderzombi102.elysium.mixin.config;

import com.enderzombi102.elysium.config.Config;
import net.minecraft.client.world.ClientWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( ClientWorld.class )
public class ClientWorldMixin {
	@Inject( method = "disconnect", at = @At( "TAIL" ) )
	public void onDisconnect( CallbackInfo ci ) {
		Config.loadFromDisk( false );
	}
}
