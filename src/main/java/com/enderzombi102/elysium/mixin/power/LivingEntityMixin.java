package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.Random;

/**
 * Used for powers: elysium:more_animal_loot
 * TODO: Use single Random
 */
@Mixin( LivingEntity.class )
public abstract class LivingEntityMixin extends Entity {

	public LivingEntityMixin( EntityType< ? > type, World world ) {
		super( type, world );
	}

	@SuppressWarnings( "ConstantValue" )
	@Inject( method = "dropLoot", at = @At( value = "INVOKE", target = "Lnet/minecraft/loot/LootTable;generateLoot(Lnet/minecraft/loot/context/LootContext;Ljava/util/function/Consumer;)V" ), locals = LocalCapture.CAPTURE_FAILHARD )
	private void dropAdditionalRancherLoot( DamageSource source, boolean causedByPlayer, CallbackInfo ci, Identifier identifier, LootTable lootTable, LootContext.Builder builder ) {
		if ( causedByPlayer && (Object) this instanceof AnimalEntity && PowerTypeRegistry.MORE_ANIMAL_LOOT.isActive( source.getAttacker() ) )
			if ( new Random().nextInt( 10 ) < 3 )
				lootTable.generateLoot( builder.build( LootContextTypes.ENTITY ), ( (LivingEntity) (Object) this )::dropStack );
	}
}
