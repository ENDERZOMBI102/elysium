package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.block.MelonBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.minecraft.block.Block.dropStacks;

/**
 * Used for powers: elysium:no_mining_exhaustion, elysium:more_crop_drops
 */
@Mixin( Block.class )
public class BlockMixin {

	@Inject( method = "afterBreak", at = @At( "TAIL" ) )
	private void dropAdditionalCrops( World world, PlayerEntity player, BlockPos pos, BlockState state, BlockEntity blockEntity, ItemStack stack, CallbackInfo ci ) {
		if ( state.getBlock() instanceof MelonBlock || ( state.getBlock() instanceof CropBlock crop && crop.isMature( state ) ) )
			if ( player != null && PowerTypeRegistry.MORE_CROP_DROPS.isActive( player ) && world.getRandom().nextInt( 10 ) < 3 )
				dropStacks( state, world, pos, blockEntity, player, stack );
	}

	@ModifyConstant( method = "afterBreak", constant = @Constant( floatValue = 0.005F ) )
	private float preventBlockMiningExhaustion( float exhaustion, World world, PlayerEntity playerEntity ) {
		if ( PowerTypeRegistry.NO_MINING_EXHAUSTION.isActive( playerEntity ) )
			return 0F;

		return exhaustion;
	}
}
