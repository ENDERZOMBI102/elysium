package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.power.RemoveRecipePower;
import io.github.apace100.apoli.component.PowerHolderComponent;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeUnlocker;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Used for powers: elysium:remove_recipes
 */
@Mixin( RecipeUnlocker.class )
public interface RecipeUnlockerMixin {
	@Inject(
		method = "shouldCraftRecipe",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/recipe/RecipeUnlocker;setLastRecipe(Lnet/minecraft/recipe/Recipe;)V",
			shift = At.Shift.BEFORE
		),
		cancellable = true
	)
	private void removeRecipe( World world, ServerPlayerEntity player, Recipe<?> recipe, CallbackInfoReturnable<Boolean> cir ) {
		var powers = player.getComponent( PowerHolderComponent.KEY ).getPowers( RemoveRecipePower.class );
		if ( powers.stream().anyMatch( power -> power.blocks( recipe.getId() ) ) )
			cir.setReturnValue( false );
	}
}
