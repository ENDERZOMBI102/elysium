package com.enderzombi102.elysium.mixin.power;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

/**
 * Used for powers: elysium:better_crafted_food, elysium:more_stone_break_speed
 */
@Mixin( Item.class )
public abstract class ItemMixin {

	@Inject( method = "appendTooltip", at = @At( "HEAD" ) )
	private void appendFoodBonusInfo( ItemStack stack, World world, List<Text> tooltip, TooltipContext context, CallbackInfo ci ) {
		if ( stack != null ) {
			NbtCompound tag = stack.hasNbt() ? stack.getNbt() : null;
			if ( tag != null ) {
				if ( tag.contains( "Elysium$FoodBonus" ) ) {
					int bonus = tag.getInt( "Elysium$FoodBonus" );
					tooltip.add( Text.translatable( "elysium.food_bonus", bonus ).formatted( Formatting.GRAY ) );
				}
				if ( tag.contains( "Elysium$MiningSpeedMultiplier" ) ) {
					int bonusInt = Math.round( ( tag.getFloat( "Elysium$MiningSpeedMultiplier" ) - 1F ) * 100 );
					String bonus = bonusInt > 0 ? ( "+" + bonusInt + "%" ) : ( bonusInt + "%" );
					tooltip.add( Text.translatable( "elysium.mining_speed_bonus", bonus ).formatted( Formatting.BLUE ) );
				}
			}
		}
	}
}
