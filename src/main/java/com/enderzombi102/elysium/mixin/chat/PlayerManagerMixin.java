package com.enderzombi102.elysium.mixin.chat;

import com.enderzombi102.elysium.config.Config;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Function;

@Mixin( PlayerManager.class )
public class PlayerManagerMixin {
	@Inject(
		method = "broadcast(Lnet/minecraft/text/Text;Ljava/util/function/Function;Z)V",
		at = @At( "HEAD" ),
		cancellable = true
	)
	public void cancelSystemMessages( Text message, Function<ServerPlayerEntity, Text> playerMessageFactory, boolean overlay, CallbackInfo ci ) {
		if (! Config.get().features.disableSystemMessages )
			return;

		if ( message.getContent() instanceof TranslatableTextContent trans && trans.getKey().startsWith( "multiplayer.player." ) )
			ci.cancel();
	}
}
