package com.enderzombi102.elysium.mixin.chat;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.command.TellRawCommand;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( TellRawCommand.class )
public class TellRawCommandMixin {
	@ModifyExpressionValue(
		method = "method_13778",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/server/command/ServerCommandSource;hasPermissionLevel(I)Z",
			ordinal = 0
		)
	)
	private static boolean addPermissions( boolean original, ServerCommandSource source ) {
		return Permissions.check( source, "elysium.tell-raw" );
	}
}
