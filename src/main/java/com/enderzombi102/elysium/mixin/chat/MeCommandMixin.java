package com.enderzombi102.elysium.mixin.chat;

import com.llamalad7.mixinextras.injector.ModifyReceiver;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.minecraft.server.command.MeCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( MeCommand.class )
public class MeCommandMixin {
	@ModifyReceiver(
		method = "register",
		at = @At(
			value = "INVOKE",
			target = "Lcom/mojang/brigadier/builder/LiteralArgumentBuilder;then(Lcom/mojang/brigadier/builder/ArgumentBuilder;)Lcom/mojang/brigadier/builder/ArgumentBuilder;",
			ordinal = 0,
			remap = false
		)
	)
	private static LiteralArgumentBuilder<ServerCommandSource> addPermissions( LiteralArgumentBuilder<ServerCommandSource> literal, ArgumentBuilder<ServerCommandSource, ?> argumentBuilder ) {
		return literal.requires( it -> Permissions.check( it, "elysium.me" ) );
	}
}
