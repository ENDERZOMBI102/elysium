package com.enderzombi102.elysium.mixin.chat;

import com.enderzombi102.elysium.registry.CompRegistry;
import com.llamalad7.mixinextras.injector.WrapWithCondition;
import net.minecraft.command.CommandSource;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( ServerCommandSource.class )
public abstract class ServerCommandSourceMixin implements CommandSource {
	@WrapWithCondition(
		method = "sendToOps",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/server/network/ServerPlayerEntity;sendMessage(Lnet/minecraft/text/Text;)V"
		)
	)
	public boolean cancelBroadcastToOp( ServerPlayerEntity target, Text text ) {
		return !target.getComponent( CompRegistry.ELYSIUM_PLAYER ).getNoCommandFeedback();
	}
}
