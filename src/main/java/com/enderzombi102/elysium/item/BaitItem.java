package com.enderzombi102.elysium.item;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static com.enderzombi102.elysium.util.Util.TOOLTIP_STYLE;

public class BaitItem extends Item {
	private final @NotNull String type;
	private final Text tooltip;

	public BaitItem( @NotNull String type ) {
		super( new Settings() );
		this.type = type;
		this.tooltip = Text.translatable( "tooltip.elysium." + type + "_bait" ).setStyle( TOOLTIP_STYLE );
	}

	@Override
	public void appendTooltip( ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context ) {
		super.appendTooltip( stack, world, tooltip, context );

		tooltip.add( this.tooltip );
	}

	public @NotNull String getType() {
		return this.type;
	}
}
