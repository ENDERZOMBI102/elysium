package com.enderzombi102.elysium.registry;

import io.github.apace100.apoli.power.Power;
import io.github.apace100.apoli.power.PowerType;
import io.github.apace100.apoli.power.PowerTypeReference;

import static com.enderzombi102.elysium.util.Const.getId;

public class PowerTypeRegistry {
	public static final PowerType<Power> LONGER_POTIONS = new PowerTypeReference<>( getId( "longer_potions" ) );
	public static final PowerType<Power> QUALITY_EQUIPMENT = new PowerTypeReference<>( getId( "quality_equipment" ) );
	public static final PowerType<Power> EFFICIENT_REPAIRS = new PowerTypeReference<>( getId( "efficient_repairs" ) );
	public static final PowerType<Power> BETTER_CRAFTED_FOOD = new PowerTypeReference<>( getId( "better_crafted_food" ) );
	public static final PowerType<Power> MORE_CROP_DROPS = new PowerTypeReference<>( getId( "more_crop_drops" ) );
	public static final PowerType<Power> BETTER_BONE_MEAL = new PowerTypeReference<>( getId( "better_bone_meal" ) );
	public static final PowerType<Power> TWIN_BREEDING = new PowerTypeReference<>( getId( "twin_breeding" ) );
	public static final PowerType<Power> MORE_ANIMAL_LOOT = new PowerTypeReference<>( getId( "more_animal_loot" ) );
	public static final PowerType<Power> NO_MINING_EXHAUSTION = new PowerTypeReference<>( getId( "no_mining_exhaustion" ) );
	public static final PowerType<Power> HIDE_NAME_PLATES = new PowerTypeReference<>( getId( "admin1_state" ) );
	public static final PowerType<Power> NO_ADMIN = new PowerTypeReference<>( getId( "no/admin" ) );
}
