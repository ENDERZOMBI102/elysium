package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.effect.MilkStatusEffect;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.util.registry.Registry;

import static com.enderzombi102.elysium.util.Const.getId;
import static com.enderzombi102.elysium.Elysium.LOGGER;

public class StatusEffectRegistry {
	public static final StatusEffect MILK_STATUS_EFFECT = new MilkStatusEffect();

	public static void register() {
		Registry.register( Registry.STATUS_EFFECT, getId( "cleanser" ), MILK_STATUS_EFFECT );

		LOGGER.info( "[Elysium] Registered status effects" );
	}
}
