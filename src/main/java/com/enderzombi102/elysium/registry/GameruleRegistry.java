package com.enderzombi102.elysium.registry;

import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.world.GameRules;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class GameruleRegistry {
	public static final GameRules.Key<GameRules.BooleanRule> DO_LAVA_FLOW = GameRuleRegistry.register(
		"doLavaFlow",
		GameRules.Category.UPDATES,
		GameRuleFactory.createBooleanRule( true )
	);


	public static void register() {
		LOGGER.info( "[Elysium] Registered game-rules" );
	}
}
