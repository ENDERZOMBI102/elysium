package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.command.*;
import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.command.CommandManager.RegistrationEnvironment;
import net.minecraft.server.command.ServerCommandSource;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class CommandRegistry {

	/**
	 * Register our commands to the server
	 */
	public static void register( CommandDispatcher<ServerCommandSource> dispatcher, CommandRegistryAccess registryAccess, RegistrationEnvironment environment ) {
		dispatcher.register( ElysiumCommand.command() );
		dispatcher.register( SpawnCommand.command() );
		dispatcher.register( TpxCommand.command() );
		if ( FabricLoader.getInstance().isDevelopmentEnvironment() )
			dispatcher.register( EldebugCommand.command() );

		LOGGER.info( "[Elysium] Registered commands" );
	}
}
