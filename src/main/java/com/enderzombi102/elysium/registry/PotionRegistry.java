package com.enderzombi102.elysium.registry;

import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.Items;
import net.minecraft.potion.Potion;
import net.minecraft.potion.Potions;
import net.minecraft.util.registry.Registry;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;
import static net.minecraft.recipe.BrewingRecipeRegistry.registerPotionRecipe;

public class PotionRegistry {
	public static final Potion CLEANSER = new Potion( "cleanser", new StatusEffectInstance( StatusEffectRegistry.MILK_STATUS_EFFECT, 1 ) );

	public static void register() {
		Registry.register( Registry.POTION, getId( "cleanser" ), CLEANSER );
		registerPotionRecipe( Potions.AWKWARD, Items.ENDER_PEARL, CLEANSER );

		LOGGER.info( "[Elysium] Registered potions" );
	}
}
