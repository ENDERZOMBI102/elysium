package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.item.BaitItem;
import com.enderzombi102.elysium.item.ClaimMonocleItem;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;


public class ItemRegistry {
	public static final Map<String, Item> ITEMS = new HashMap<>() {{
		put( "race_dwarf"     , new Item( new Item.Settings() ) );
		put( "race_elf"       , new Item( new Item.Settings() ) );
		put( "race_halforc"   , new Item( new Item.Settings() ) );
		put( "race_human"     , new Item( new Item.Settings() ) );
		put( "role_admin"     , new Item( new Item.Settings() ) );
		put( "role_alchemist" , new Item( new Item.Settings() ) );
		put( "role_archer"    , new Item( new Item.Settings() ) );
		put( "role_bard"      , new Item( new Item.Settings() ) );
		put( "role_blacksmith", new Item( new Item.Settings() ) );
		put( "role_carpenter" , new Item( new Item.Settings() ) );
		put( "role_cleric"    , new Item( new Item.Settings() ) );
		put( "role_cook"      , new Item( new Item.Settings() ) );
		put( "role_countryman", new Item( new Item.Settings() ) );
		put( "role_nitwit"    , new Item( new Item.Settings() ) );
		put( "role_thief"     , new Item( new Item.Settings() ) );
		put( "role_warrior"   , new Item( new Item.Settings() ) );
		put( "role_wizard"    , new Item( new Item.Settings() ) );
		put( "role_worker"    , new Item( new Item.Settings() ) );

		put( "fish_bait"      , new BaitItem( "fish" ) );
		put( "animal_bait"    , new BaitItem( "animal" ) );

		put( "claim_monocle" , new ClaimMonocleItem( new Item.Settings().maxCount( 1 ) ) );
	}};

	public static void register() {
		for ( var entry : ITEMS.entrySet() )
			Registry.register( Registry.ITEM, getId( entry.getKey() ), entry.getValue() );

		LOGGER.info( "[Elysium] Registered items" );
	}
}
