package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.component.*;
import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import dev.onyxstudios.cca.api.v3.level.LevelComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.level.LevelComponentInitializer;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

@SuppressWarnings( "UnstableApiUsage" )
public class CompRegistry implements EntityComponentInitializer, LevelComponentInitializer {
	public static final ComponentKey<ElysiumPlayerComponent> ELYSIUM_PLAYER =
		ComponentRegistry.getOrCreate( getId( "elysium_player" ), ElysiumPlayerComponent.class );
	public static final ComponentKey<ImperiumLevelComponent> IMPERIUM_LEVEL =
		ComponentRegistry.getOrCreate( getId( "imperium_level" ), ImperiumLevelComponent.class );
	public static final ComponentKey<ElysiumLevelComponent> ELYSIUM_LEVEL =
		ComponentRegistry.getOrCreate( getId( "elysium_level" ), ElysiumLevelComponent.class );

	@Override
	public void registerEntityComponentFactories( EntityComponentFactoryRegistry registry ) {
		registry.registerForPlayers( ELYSIUM_PLAYER, ElysiumPlayerComponentImpl::new );

		LOGGER.info( "[Elysium] Registered entity components" );
	}

	@Override
	public void registerLevelComponentFactories( LevelComponentFactoryRegistry registry ) {
		registry.register( IMPERIUM_LEVEL, ImperiumLevelComponentImpl::new );
		registry.register( ELYSIUM_LEVEL, ElysiumLevelComponentImpl::new );

		LOGGER.info( "[Elysium] Registered level components" );
	}
}
