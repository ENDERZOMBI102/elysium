package com.enderzombi102.elysium.effect;

import com.enderzombi102.elysium.util.Util;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.InstantStatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.stream.Collectors;

public class MilkStatusEffect extends InstantStatusEffect {
	public MilkStatusEffect() {
		super( StatusEffectCategory.NEUTRAL, Util.toColor( 58, 63, 38, 40 ) );
	}

	@Override
	public void applyInstantEffect( @Nullable Entity source, @Nullable Entity attacker, LivingEntity target, int amplifier, double proximity ) {
		Set<StatusEffect> compatibleEffectList = target.getStatusEffects()
			.stream()
			.map( StatusEffectInstance::getEffectType )
			.filter( effectType -> effectType.getCategory() == StatusEffectCategory.HARMFUL )
			.collect( Collectors.toUnmodifiableSet() );

		if (! compatibleEffectList.isEmpty() )
			compatibleEffectList.stream()
				.skip( target.getWorld().getRandom().nextInt( compatibleEffectList.size() ) )
				.findFirst()
				.ifPresent( target::removeStatusEffect );
	}
}
