package com.enderzombi102.elysium;


import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.feature.AntiCheatFeature;
import com.enderzombi102.elysium.feature.ClaimFeature;
import com.enderzombi102.elysium.feature.DimensionalGamemodeFeature;
import com.enderzombi102.elysium.registry.*;
import io.github.apace100.origins.origin.Origin;
import io.github.apace100.origins.origin.OriginRegistry;
import io.github.apace100.origins.registry.ModComponents;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.resource.LifecycledResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;

public class Elysium implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger( "Elysium" );
	public static @Nullable MinecraftServer server = null;
	public static boolean reloading = false;

	public void onInitialize() {
		ServerLifecycleEvents.END_DATA_PACK_RELOAD.register( Elysium::onServerReloadEnd );
		ServerLifecycleEvents.START_DATA_PACK_RELOAD.register( ( server, resourceManager ) -> reloading = true );
		CommandRegistrationCallback.EVENT.register( CommandRegistry::register );
		ServerLifecycleEvents.SERVER_STARTED.register( mc -> server = mc );

		// registries
		PowerRegistry.register();
		BlockConditionRegistry.register();
		StatusEffectRegistry.register();
		ItemRegistry.register();
		ScreenRegistry.register();
		GameruleRegistry.register();
//		PotionRegistry.register();

		// features
		ClaimFeature.register();
		DimensionalGamemodeFeature.register();
		AntiCheatFeature.register();
	}

	private static void onServerReloadEnd( MinecraftServer server, LifecycledResourceManager manager, boolean success ) {
		if (! success )
			return;

		var players = server.getPlayerManager().getPlayerList();

		Config.loadFromDisk( true );

		LOGGER.info( "[Elysium] Sending config update to {} players", players.size() );
		for ( var player : players )
			ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBuf() );

		if ( Config.get().features.reloadOrigins ) {
			for ( var player : players ) {
				var component = player.getComponent( ModComponents.ORIGIN );
				var layers = new ArrayList<>( component.getOrigins().keySet() );
				var origins = new ArrayList<>( component.getOrigins().values() );

				for ( var i = 0; i < layers.size(); i += 1 ) {
					component.setOrigin( layers.get( i ), Origin.EMPTY );
					component.setOrigin( layers.get( i ), OriginRegistry.get( origins.get( i ).getIdentifier() ) );
				}

				component.sync();
			}

			LOGGER.info( "[Elysium] Reloaded origins from disk" );
			server.getPlayerManager().broadcast( Text.literal( "[Elysium] Reloaded origins from disk" ), false );
		}

		reloading = false;
	}
}
