package com.enderzombi102.elysium.config;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import blue.endless.jankson.JsonPrimitive;
import blue.endless.jankson.api.DeserializationException;
import blue.endless.jankson.api.SyntaxError;
import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.util.Const;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;

public class Config {
	private static final Jankson JANKSON = Jankson.builder()
		.registerDeserializer(
			String.class, BlockPos.class,
			( it, marshaller ) -> {
				var arr = it.split( ", " );
				assert arr.length == 3 : "Invalid BlockPos construct";
				return new BlockPos( Double.parseDouble( arr[0] ), Double.parseDouble( arr[1] ), Double.parseDouble( arr[2] ) );
			}
		)
		.registerSerializer( BlockPos.class, ( it, marshaller ) -> JsonPrimitive.of( it.toShortString() ) )
		.build();
	private static final JsonGrammar GRAMMAR = JsonGrammar.builder()
		.withComments( true )
		.printTrailingCommas( true )
		.bareSpecialNumerics( true )
		.printUnquotedKeys( true )
		.build();
	private static final File CONFIG_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-tweaks.json5" ).toFile();
	private static @Nullable ConfigData DATA = null;
	private static @Nullable ConfigFrom SOURCE = null;


	private Config() { }

	public static @NotNull ConfigData get() {
		if ( DATA == null )
			loadFromDisk( false );

		return DATA;
	}

	public static void save() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving config to disk..." );
			Files.writeString( CONFIG_FILE.toPath(), JANKSON.toJson( DATA ).toJson( GRAMMAR ) );
			var server = Elysium.server;
			if ( server != null && server.isDedicated() ) {
				LOGGER.info( "[Elysium] Config saved, sending to clients!" );
				for ( var player : server.getPlayerManager().getPlayerList() )
					ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBuf() );
			} else
				LOGGER.info( "[Elysium] Config saved" );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save config to disk: ", e );
		}
	}

	public static void loadFromDisk( boolean reloading ) {
		if ( reloading )
			LOGGER.info( "[Elysium] Reloading config..." );
		else
			LOGGER.info( "[Elysium] Loading config..." );

		if (! CONFIG_FILE.exists() ) {
			LOGGER.info( "[Elysium] Config does not exist, creating..." );
			DATA = new ConfigData();
			SOURCE = ConfigFrom.File;
			save();
			return;
		}

		try {
			DATA = JANKSON.fromJsonCarefully( JANKSON.load( CONFIG_FILE ), ConfigData.class );
			SOURCE = ConfigFrom.File;

			if ( reloading )
				LOGGER.info( "[Elysium] Config reloaded" );
			else
				LOGGER.info( "[Elysium] Config loaded" );
		} catch ( DeserializationException | SyntaxError | IOException e ) {
			LOGGER.error( "[Elysium] Failed to load config: ", e );
		}
	}

	public static void loadFromPacket( @NotNull String version, @NotNull String json ) {
		LOGGER.info( "[Elysium] Loading config received from server" );

		if (! version.equals( Const.VERSION ) )
			LOGGER.warn( "[Elysium] Config received from server has a different mod version attached: %s (ours) vs %s (theirs)".formatted( Const.VERSION, version ) );

		try {
			DATA = JANKSON.fromJson( JANKSON.load( json ), ConfigData.class );
			SOURCE = ConfigFrom.Server;
			LOGGER.info( "[Elysium] Loaded config from server" );
		} catch ( SyntaxError e ) { throw new RuntimeException( e ); }
	}

	public static @NotNull PacketByteBuf getPacketBuf() {
		return PacketByteBufs.create()
			.writeString( Const.VERSION )
			.writeString( JANKSON.toJson( DATA ).toJson( JsonGrammar.COMPACT ) );
	}

	private enum ConfigFrom {
		File,
		Server
	}
}
