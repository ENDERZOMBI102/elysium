package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;

public class LoggingData {
	@Comment( "Additional logging for the claim anchor taxes mechanic" )
	public boolean claimAnchor = false;
	@Comment( "Additional logging for the claim anchor's screen" )
	public boolean claimAnchorScreen = false;

	@Comment( "Additional logging for the virtual resource pack" )
	public boolean virtualResourcePack = false;

	@Comment( "Additional logging for the changing of a player's gamemode when changing dimension" )
	public boolean dimensionalGamemode = false;

	@Comment( "Additional logging for the blocking of experience" )
	public boolean experienceBlocker = false;
}
