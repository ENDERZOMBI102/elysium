package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AntiCheatData {
	@Comment("""
	An array of blocked or !required mod ids.
	Example:
	```json5
	mods: [ "journeymap", "!elysium" ],
	```
	""")
	public @NotNull List<String> mods = new ArrayList<>();
	@Comment("""
	An array of blocked or !required classes.
	Example:
	```json5
	classes: [ "journeymap.client.JourneymapClient", "!com.enderzombi102.elysium.Elysium", ],
	```
	""")
	public @NotNull List<String> classes = new ArrayList<>();
}
