package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;

public class FeatureSwitchData {
	@Comment( "Reload players origins when the server reload the datapacks" )
	public boolean reloadOrigins = true;

	@Comment( "Invasive modifications to goml's claim system to allow taxes ( Using new blocks is too easy :3 )" )
	public boolean claimModifications = true;

	@Comment( "DreamingCode's edits to goml, reimplemented as mixins" )
	public boolean dreamingGomlFixes = true;

	@Comment( "Disables the spawning of mobs in the overworld" )
	public boolean disableMobSpawning = true;

	@Comment( "Toggles the forced gamemode upon changing dimension" )
	public boolean forceDimensionalGamemode = true;

	@Comment( "Disable exp gathering and most of its usages" )
	public boolean disableExperience = true;

	@Comment( "Disable the player list" )
	public boolean disablePlayerList = true;

	@Comment( "Disable the join/leave messages" )
	public boolean disableSystemMessages = true;

	@Comment( "Check client's mods before starting/login, not allowing if fails" )
	public boolean modListCheck = true;
}
