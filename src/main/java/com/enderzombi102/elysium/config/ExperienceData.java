package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ExperienceData {
	@Comment( "Blocks the dropping of xp in this dimension" )
	public boolean enable = true;

	@Comment( "Allow furnaces to drop xp" )
	public boolean allowFurnaceExp = true;

	@Comment( "Allow xp bottles to drop xp" )
	public boolean allowBottledExp = true;

	@Comment( "Array of mob ids allowed to drop xp anyway,\nyou can use `elysium:hostile` and `elysium:animal` to have a sort of \"wildcard allow\"" )
	public @NotNull List<String> allowedMobSources = new ArrayList<>();
}
