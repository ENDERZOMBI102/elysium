package com.enderzombi102.elysium.component;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;

public interface ImperiumLevelComponent extends ComponentV3 {
	long getBalance();
	long addBalance( long amount );
}
