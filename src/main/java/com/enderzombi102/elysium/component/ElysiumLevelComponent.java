package com.enderzombi102.elysium.component;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public interface ElysiumLevelComponent extends ComponentV3 {
	// Player time
	long getLastPlayerTime( @NotNull UUID player );
	void setLastPlayerTime( @NotNull UUID player, long amount );

	// Claim anchor timestamp backups
	long getAnchorStamp( @NotNull UUID owner );
	void setAnchorStamp( @NotNull UUID owner, long stamp );
}
