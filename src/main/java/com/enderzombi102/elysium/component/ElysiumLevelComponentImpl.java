package com.enderzombi102.elysium.component;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.WorldProperties;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ElysiumLevelComponentImpl implements ElysiumLevelComponent {
	private final Map<UUID, Long> playerTimes = new HashMap<>();
	private final Map<UUID, Long> anchorStamps = new HashMap<>();

	public ElysiumLevelComponentImpl( WorldProperties props ) { }

	@Override
	public long getLastPlayerTime( @NotNull UUID player ) {
		return this.playerTimes.getOrDefault( player, 0L );
	}

	@Override
	public void setLastPlayerTime( @NotNull UUID player, long time ) {
		this.playerTimes.put( player, time );
	}


	@Override
	public long getAnchorStamp( @NotNull UUID owner ) {
		return this.anchorStamps.getOrDefault( owner, 0L );
	}

	@Override
	public void setAnchorStamp( @NotNull UUID owner, long stamp ) {
		this.anchorStamps.put( owner, stamp );
	}


	@Override
	public void readFromNbt( NbtCompound nbt ) {
		this.playerTimes.clear();
		this.anchorStamps.clear();

		var map = nbt.getCompound( "playerTimes" );
		for ( var key : map.getKeys() )
			this.playerTimes.put( UUID.fromString( key ), map.getLong( key ) );

		map = nbt.getCompound( "anchorStamps" );
		for ( var key : map.getKeys() )
			this.anchorStamps.put( UUID.fromString( key ), map.getLong( key ) );
	}

	@Override
	public void writeToNbt( NbtCompound nbt ) {
		var map = new NbtCompound();
		for ( var entry : this.playerTimes.entrySet() )
			map.putLong( entry.getKey().toString(), entry.getValue() );

		nbt.put( "playerTimes", map );

		map = new NbtCompound();
		for ( var entry : this.anchorStamps.entrySet() )
			map.putLong( entry.getKey().toString(), entry.getValue() );

		nbt.put( "anchorStamps", map );
	}
}
