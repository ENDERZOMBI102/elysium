package com.enderzombi102.elysium.component;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.WorldProperties;

public class ImperiumLevelComponentImpl implements ImperiumLevelComponent {
	private long balance = 0;

	public ImperiumLevelComponentImpl( WorldProperties props ) {

	}

	@Override
	public long getBalance() {
		return this.balance;
	}

	@Override
	public synchronized long addBalance( long amount ) {
		this.balance += amount;

		if ( this.balance < 0 )
			this.balance = 0;

		return this.balance;
	}

	@Override
	public void readFromNbt( NbtCompound nbt ) {
		this.balance = nbt.getLong( "balance" );
	}

	@Override
	public void writeToNbt( NbtCompound nbt ) {
		nbt.putLong( "balance", this.balance );
	}
}
