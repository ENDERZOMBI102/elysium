package com.enderzombi102.elysium.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.command.argument.TextArgumentType;
import net.minecraft.server.command.ServerCommandSource;

import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class EldebugCommand {
	public static LiteralArgumentBuilder<ServerCommandSource> command() {
		return literal( "eldebug" )
			.then( literal( "crash" )
				.executes( EldebugCommand::crash )
			)
			.then( literal( "broadcast" )
				.then( argument( "message", TextArgumentType.text() )
					.executes( EldebugCommand::broadcast )
				)
			);
	}

	private static int crash( CommandContext<ServerCommandSource> ctx ) {
		var server = ctx.getSource().getServer();
		server.executeSync( () -> server.stop( true ) );

		if ( FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT ) {
			var client = MinecraftClient.getInstance();
			client.executeSync( client::scheduleStop );
		}

		return 1;
	}

	private static int broadcast( CommandContext<ServerCommandSource> ctx ) {
		ctx.getSource()
			.getServer()
			.getPlayerManager()
			.broadcast( TextArgumentType.getTextArgument( ctx, "message" ), false );

		return 1;
	}
}

