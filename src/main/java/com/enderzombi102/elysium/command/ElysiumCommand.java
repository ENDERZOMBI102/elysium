package com.enderzombi102.elysium.command;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.component.ImperiumLevelComponent;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.command.argument.BlockPosArgumentType;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;

import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;
import static com.enderzombi102.elysium.util.Util.position;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class ElysiumCommand {
	public static LiteralArgumentBuilder<ServerCommandSource> command() {
		return literal( "elysium" )
			.requires( it -> Permissions.check( it, "elysium.elysium", 2 ) )
			.then( literal( "config" )
				.then( literal( "reload" )
					.executes( ElysiumCommand::configReload )
				)
				.then( literal( "spawn" )
					.then( argument( "pos", BlockPosArgumentType.blockPos() )
						.executes( ElysiumCommand::configSpawnSet )
					)
					.executes( ElysiumCommand::configSpawn )
				)
			)
			.then( literal( "no-command" )
				.executes( ElysiumCommand::toggleNoCommandFeedback )
			)
			.then( literal( "claim" )
				.then( literal( "get-time" )
					.then( argument( "owner", EntityArgumentType.player() )
						.executes( ElysiumCommand::claimGetTimePlayer )
					)
				)
			)
			.then( literal( "balance" )
				.then( literal( "imperium" )
					.executes( ElysiumCommand::balanceImperiumDisplay )
					.then( literal( "add" )
						.then( argument( "amount", IntegerArgumentType.integer( 0 ) )
							.executes( ElysiumCommand::balanceImperiumAdd )
						)
					)
					.then( literal( "remove" )
						.then( argument( "amount", IntegerArgumentType.integer( 0 ) )
							.executes( ElysiumCommand::balanceImperiumRemove )
						)
					)
				)
			);
	}

	private static int configReload( CommandContext<ServerCommandSource> ctx ) {
		Config.loadFromDisk( true );

		var players = ctx.getSource().getServer().getPlayerManager().getPlayerList();
		Elysium.LOGGER.info( "[Elysium] Sending config update to {} players", players.size() );
		for ( var player : players )
			ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBuf() );

		ctx.getSource().sendFeedback( Text.literal( "Config reloaded!" ), false );
		return 1;
	}

	private static int configSpawnSet( CommandContext<ServerCommandSource> ctx ) throws CommandSyntaxException {
		var pos = BlockPosArgumentType.getBlockPos( ctx, "pos" );
		var old = Config.get().claims.spawnPosition;

		var message = Text.literal( "Spawn has been set to " ).append( position( pos ) );

		if ( old != null )
			message.append( Text.literal( ", was " ) ).append( position( old ) );

		Config.get().claims.spawnPosition = pos;
		Config.save();

		ctx.getSource().sendFeedback( message, false );

		return 1;
	}

	private static int configSpawn( CommandContext<ServerCommandSource> ctx ) {
		var pos = Config.get().claims.spawnPosition;

		var message = pos == null ?
			Text.literal( "Spawn is not set" ) :
			Text.literal( "Spawn is at " ).append( position( pos ) );

		ctx.getSource().sendFeedback( message, false );

		return 1;
	}

	private static int toggleNoCommandFeedback( CommandContext<ServerCommandSource> ctx ) throws CommandSyntaxException {
		var comp = ctx.getSource()
			.getPlayerOrThrow()
			.getComponent( CompRegistry.ELYSIUM_PLAYER );

		comp.setNoCommandFeedback( !comp.getNoCommandFeedback() );

		return 1;
	}

	private static int claimGetTimePlayer( CommandContext<ServerCommandSource> ctx ) throws CommandSyntaxException {
		var player = EntityArgumentType.getPlayer( ctx, "owner" );

		assert Elysium.server != null : "y nu sevr";
		var stamp = CompRegistry.ELYSIUM_LEVEL
			.get( Elysium.server.getSaveProperties() )
			.getAnchorStamp( player.getUuid() );

		ctx.getSource()
			.sendFeedback(
				Text.literal( player.getEntityName() + "'s last saved time is " + Instant.ofEpochMilli( stamp ) ),
				false
			);

		return 0;
	}

	private static int balanceImperiumDisplay( CommandContext<ServerCommandSource> ctx ) {
		var comp = getImperium( ctx );
		var bronze = comp.getBalance();

		var gold = bronze / 10000L;
		bronze = bronze % 10000L;

		var silver = bronze / 100L;
		bronze = bronze % 100L;

		var text = Text.translatable(
			"command.elysium.balance.display",
			"Imperium's",
			gold,
			silver,
			bronze,
			comp.getBalance()
		);
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static int balanceImperiumAdd( CommandContext<ServerCommandSource> ctx ) {
		var amount = IntegerArgumentType.getInteger( ctx, "amount" );

		var comp = getImperium( ctx );
		var balance = comp.addBalance( amount );

		var text = Text.translatable( "command.elysium.balance.add", amount, balance );
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static int balanceImperiumRemove( CommandContext<ServerCommandSource> ctx ) {
		var amount = IntegerArgumentType.getInteger( ctx, "amount" );

		var comp = getImperium( ctx );
		var balance = comp.addBalance( -amount );

		var text = Text.translatable( "command.elysium.balance.remove", amount, balance );
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static @NotNull ImperiumLevelComponent getImperium( CommandContext<ServerCommandSource> ctx ) {
		return CompRegistry.IMPERIUM_LEVEL.get( ctx.getSource().getServer().getSaveProperties() );
	}
}
