package com.enderzombi102.elysium.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;

import static net.minecraft.server.command.CommandManager.literal;

public class SpawnCommand {
	public static LiteralArgumentBuilder< ServerCommandSource > command() {
		return literal( "spawn" )
			.requires( it -> Permissions.check( it, "elysium.spawn", 2 ) )
			.executes( ctx -> {
				var destination = ctx.getSource().getServer().getWorld( ServerWorld.OVERWORLD );

				if ( destination == null )
					return 0;

				var pos = destination.getSpawnPos();
				var player = ctx.getSource().getPlayerOrThrow();
				player.teleport( destination, pos.getX(), pos.getY(), pos.getZ(), player.headYaw, player.getPitch() );

				return 1;
			});
	}
}
