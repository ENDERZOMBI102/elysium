package com.enderzombi102.elysium.util;

import draylar.goml.api.Claim;
import draylar.goml.registry.GOMLBlocks;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.mehvahdjukaar.supplementaries.common.items.QuiverItem;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;

public class Util {
	public static final Style TOOLTIP_STYLE = Style.EMPTY.withFormatting( Formatting.DARK_GRAY ).withItalic( true );
	public static final ModContainer CONTAINER = FabricLoader.getInstance().getModContainer( Const.ID ).orElseThrow();

	public static int toColor( int red, int green, int blue, int alpha ) {
		return ( ( alpha & 0xFF ) << 24 ) | ( ( red & 0xFF ) << 16 ) | ( ( green & 0xFF ) << 8 ) | ( blue & 0xFF );
	}

	public static @NotNull Text position( @NotNull BlockPos pos ) {
		return Text.literal( pos.toShortString().replace( ", ", " " ) )
			.setStyle(
				Style.EMPTY
					.withClickEvent(
						new ClickEvent(
							ClickEvent.Action.RUN_COMMAND,
							"/tp @s %s %s %s".formatted( pos.getX(), pos.getY(), pos.getZ() )
						)
					)
					.withFormatting( Formatting.GREEN )
			);
	}

	public static Path getJarPath() {
		var root = CONTAINER.getRoot();
		var path = root.toUri().toString();

		if ( path.startsWith( "jar:file" ) )
			root = Path.of( path.substring( 12, path.length() - 2 ) );

		return root;
	}

	public static @Nullable QuiverItem.Data getQuiverData( @NotNull PlayerEntity player ) {
		for ( var stack : player.getInventory().main )
			if ( stack.getItem() instanceof QuiverItem )
				return QuiverItem.getQuiverData( stack );

		for ( var stack : player.getInventory().offHand )
			if ( stack.getItem() instanceof QuiverItem )
				return QuiverItem.getQuiverData( stack );

		return null;
	}

	public static boolean isOverworld( @NotNull ServerWorld world ) {
		return world == world.getServer().getWorld( ServerWorld.OVERWORLD );
	}

	public static boolean isAdminClaim( @NotNull BlockState claim ) {
		return claim.getBlock() == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst();
	}

	public static boolean isAdminClaim( @NotNull Claim claim ) {
		return claim.getType() == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst();
	}
}
