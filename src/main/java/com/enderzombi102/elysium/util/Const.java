package com.enderzombi102.elysium.util;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class Const {
	public static final @NotNull String ID = "elysium";
	public static final @NotNull String VERSION = FabricLoader.getInstance()
		.getModContainer( ID )
		.orElseThrow()
		.getMetadata()
		.getVersion()
		.getFriendlyString();
	public static final @NotNull Identifier CONFIG_SYNC_ID = getId( "config_sync" );
	public static final @NotNull Identifier CLAIM_OUTLINES_ID = getId( "claim_outlines" );
	public static final @NotNull Identifier CLAIM_CLOSEST_ID = getId( "claim_closest" );
	public static final @NotNull Identifier CLAIM_ADMIN_MODER_ID = getId( "claim_admin_moder" );

	public static @NotNull Identifier getId( @NotNull String path ) {
		return new Identifier( ID, path );
	}
}
