package com.enderzombi102.elysium.util;

import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.stream.Collectors;

public class DoorUtil {
	public static void fixOpeningDoor( World world, BlockPos pos, PlayerEntity player, CallbackInfoReturnable<ActionResult> cir ) {
		if ( world.isClient() ) {
			cir.setReturnValue( ActionResult.SUCCESS );
			return;
		}

		var claims = ClaimUtils.getClaimsAt( world, pos ).collect( Collectors.toList() );

		if ( claims.isEmpty() || claims.get( 0 ).getValue().hasPermission( player ) || ClaimUtils.isInAdminMode( player ) )
			return;

		cir.setReturnValue( ActionResult.FAIL );
	}
}
