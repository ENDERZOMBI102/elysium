package com.enderzombi102.elysium.util;

import net.minecraft.client.util.math.MatrixStack;
import org.jetbrains.annotations.NotNull;

// TODO: Move to library
public class MatrixStackLayer implements AutoCloseable {
	private final @NotNull MatrixStack stack;

	public MatrixStackLayer( @NotNull MatrixStack stack ) {
		this.stack = stack;
		this.stack.push();
	}

	@Override
	public void close() {
		this.stack.pop();
	}
}
