package com.enderzombi102.elysium.util;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import draylar.goml.api.Claim;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.enderzombi102.elysium.util.Util.isAdminClaim;

public class TaxesUtil {

	/**
	 * Calculates the distance from the specified claim anchor pos to the spawn.
	 * @param claimAnchorPos anchor to calculate the distance of.
	 * @return the distance.
	 */
	public static int getDistance( @NotNull BlockPos claimAnchorPos ) {
		var center = Config.get().claims.spawnPosition;

		if ( center == null )
			return 0;

		return Math.max(
			Math.abs( center.getX() - claimAnchorPos.getX() ),
			Math.abs( center.getZ() - claimAnchorPos.getZ() )
		);
	}

	/**
	 * Calculates the cost of the claim.
	 * @param claimAnchorPos the position of the claim anchor.
	 * @return the claim cost daily using Apfloat precision.
	 */
	public static @NotNull Apfloat getClaimPup( @NotNull BlockPos claimAnchorPos ) {
		var distance = getDistance( claimAnchorPos ) - Config.get().claims.distanceRadiusBetween;

		return ApfloatMath.max( Apfloat.ONE, calculateExponentialCost( distance ) );
	}

	/**
	 * Calculates the exponential cost of the claim based on the distance from the claim anchor position.
	 * @param distance the distance from the claim anchor position.
	 * @return the exponential claim cost.
	 */
	private static @NotNull Apfloat calculateExponentialCost( int distance ) {
		var distance2 = distance / 5;

		var exponentialCost = new Apfloat( Config.get().claims.taxes.maximumFee )
			.divide(
				ApfloatMath.pow(
					Apfloat.ONE
						.add( new Apfloat( "3.6" ).divide( new Apfloat( 100 ) ) ),
					distance2
				)
			);

		var minimumFee = new Apfloat( Config.get().claims.taxes.minimumFee );
		return exponentialCost.compareTo( minimumFee ) < 0 ? minimumFee : exponentialCost;
	}

	/**
	 * Checks whether a claim is considered as 'disabled'.
	 * @param claim the claim to check.
	 * @param world the world the claim is in.
	 * @return true if it is, false otherwise.
	 */
	public static boolean isClaimDisabled( Claim claim, ServerWorld world ) {
		var anchor = (ClaimAnchorBlockEntity) world.getBlockEntity( claim.getOrigin() );
		assert anchor != null : "Claim without anchor block entity??";

		var block = Objects.requireNonNull( anchor.getWorld() )
			.getBlockState( anchor.getPos() );

		var mixin = (ElysiumClaimBlockEntity) anchor;

		return (! isAdminClaim( block ) ) && isCreditsExpired( mixin );
	}

	/**
	 * Checks whether the credits for a claim are expired.
	 * @param anchor the ElysiumClaimBlockEntity representing the claim.
	 * @return true if the credits are expired, false otherwise.
	 */
	private static boolean isCreditsExpired( ElysiumClaimBlockEntity anchor ) {
		return anchor.elysium$getTime() < System.currentTimeMillis();
	}
}
