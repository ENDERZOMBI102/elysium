<h1 style="text-align: center">
    Elysium Tweaks
    <br/>
    <a href="https://www.curseforge.com/minecraft/mc-mods/elysium-tweaks">
        <img src="https://img.shields.io/badge/-CurseForge-gray?style=for-the-badge&logo=curseforge&labelColor=orange" alt="curseforge"/>
    </a>
    <a href="https://modrinth.com/mod/elysium-tweaks">
        <img src="https://img.shields.io/badge/-modrinth-gray?style=for-the-badge&labelColor=green&labelWidth=15&logo=modrinth&logoColor=white" alt="modrinth"/>
    </a>
</h1>

Contains tweaks, blocks and items for the ElysiumCraft modpack and server.

This is a fully configurable mod for the ElysiumCraft server, it is used to (heavily) tweak the game, current features:
- Ability to reload origins when the server is reloaded
- Heavily modify the GOML's claim system to allow taxes
- Disable mob spawning
- Force a dimension's gamemode
- Disable the experience mechanic, with ability to select allowed sources
- Fix supplementaries's Quiver and add more mod support to it
- Add more Apoli powers
- Make the `me`, `tell`, `tellraw`, `msg` and `w` commands require a permission
- Ability to disable the `TAB` player list overlay
- Fix doors and trapdoors being able to be opened in a claim
- Installed client mods check for servers

### Permissions
- `elysium.spawn` ( defaults to permission level 2 )
- `elysium.dimension_bypass`
- `elysium.elysium` ( defaults to permission level 2 )
- `elysium.tpx` ( defaults to permission level 2 )
- `elysium.me`
- `elysium.msg`
- `elysium.tell-raw`
- `elysium.bypass_mod_check`


